# Songs Database

Tools for querying the Rocksmith+ songs database.

## Installation

Download the latest release from the "Releases" section.

## Random Song

The random song script allows you to select your region and the type of arrangement you are looking for, then it returns a random song available for you to play in Rocksmith+.

### Usage

Simply run the executable file

    random_song.exe

## Build Requirements

- Python v3.10.5
- pandas
- tktooltip
- sv-ttk

## Build Instructions

The project is built into an executable using "pyinstaller"

    pyinstaller -w --add-data "rs_plus_regions/songs.db;rs_plus_regions" --collect-data sv_ttk random_song.py

This will create a "dist" folder containing everything required to run the project in a self-contained package.

## Limitations

Currently the song database is a bit out of date. I have reached out to the maintainer of https://github.com/scapula/rs_plus_regions to inquire about getting the database up-to-date.