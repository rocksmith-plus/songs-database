from tkinter import *
from tkinter import ttk
from tktooltip import ToolTip

import pandas as pd
import random
import sqlite3
import sv_ttk

# Establish the database connection
conn = sqlite3.connect("rs_plus_regions/songs.db")

# Get list of available regions
regions = pd.read_sql("""
SELECT country FROM regions
""", conn)
countries = regions['country'].drop_duplicates().to_list()

# Set window title and size
root = Tk()
root.title("Random Song Selector")
root.geometry("380x125")

# Set theme
sv_ttk.set_theme("dark")

# Define the form/layout
frm = ttk.Frame(root, padding=10)
frm.grid()

# Define country selection dropdown
ttk.Label(frm, text="Country").grid(sticky=E, column=0, row=0, padx=10, pady=10)
country = StringVar()
ttk.OptionMenu(frm, country, countries[0], *countries).grid(sticky=W, column=1, row=0)

# Define filter dropdown
arrangements = [
  'Lead',
  'Rhythm',
  'Bass',
]
ttk.Label(frm, text="Arrangement").grid(sticky=E, column=0, row=1, padx=10, pady=10)
arrangement = StringVar()
ttk.OptionMenu(frm, arrangement, arrangements[0], *arrangements).grid(sticky=W, column=1, row=1)

# Define "Go" button
def GetRandomSong():

  global song_label

  arrangementQuery = ""
  if arrangement.get() == "Lead":
    arrangementQuery = "songs.lead = 1 OR songs.altLead = 1"
  elif arrangement.get() == "Rhythm":
    arrangementQuery = "songs.rhythm = 1 OR songs.altRhythm = 1"
  elif arrangement.get() == "Bass":
    arrangementQuery = "songs.bass = 1 OR songs.altBass = 1"

  countryQuery = "regions.country = '" + country.get() + "'"

  songs = pd.read_sql(f"""
  SELECT artistName, songName FROM songs
  INNER JOIN regions ON songs.songId = regions.songId
  WHERE {arrangementQuery} AND {countryQuery}
  """, conn)

  random_index = random.randint(0, len(songs) - 1)

  song = songs.iloc[random_index]["artistName"] + " - " + songs.iloc[random_index]["songName"]

  # Truncate to 50 characters
  song_maxlen = 40
  song_truncated = song
  if (len(song_truncated) > song_maxlen):
    song_truncated = song_truncated[:song_maxlen-3] + '...'

  song_label['text'] = song_truncated
  song_tooltip.msg = song

ttk.Button(frm, text="Go", command=GetRandomSong).grid(sticky=W, column=0, row=2)

# Define output label
song_label = ttk.Label(frm, text="")
song_label.grid(sticky=W, column=1, row=2)

# Bind a tooltip containing the full song name
song_tooltip = ToolTip(song_label, msg="")

root.mainloop()